# OpenEDI

#### Description
An open-source infrastructure for VLSI design automation tool set.  

#### Software Architecture
Key components include:
- Database - OpenEDI database, core module.
- Parser - parse standard data format and import data to OpenEDI databade.
- CommandManager - manage commands.
- MessageManager - manage messages.
- IOManager - High-performance file IO.
- GUI - Graphic user interface


#### Building Dependency (Recommend)

1. [GCC 6.5.0](http://ftp.tsukuba.wide.ad.jp/software/gcc/releases/gcc-6.5.0/gcc-6.5.0.tar.gz)

2. [Cmake 3.12.4](https://cmake.org/files/v3.12/cmake-3.12.4-Linux-x86_64.tar.gz)

3. [Qt 5.9.7](http://download.qt.io/archive/qt/5.9/5.9.7/qt-opensource-linux-x64-5.9.7.run)

4. Python 3.6

   Recommend to use [anaconda](https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/Anaconda3-5.2.0-Linux-x86_64.sh)

5. [Google test](https://gitee.com/mirrors/googletest)

   - Integrated as a submodule

6. [Pybind11](https://gitee.com/mirrors/pybind11)

   - Integrated as a submodule

7. [Boost](https://www.boost.org) 1.69

   - Must be found by CMake
   - Recommend to use [anaconda](https://anaconda.org) to install the library by `conda install -c anaconda boost`
   - Then `export BOOST_ROOT=/path/to/anaconda` so that CMake can find boost
   - If Boost is installed with other approaches, you can export `BOOST_ROOT` to wherever it is installed

8. [Tcl 8.6.9](https://downloads.sourceforge.net/tcl/tcl8.6.9-src.tar.gz)

9. Flex 2.5.37+

10. Bison 3.0.4+

11. [Gperftools 2.7.90](https://github.com/gperftools/gperftools/releases/download/gperftools-2.7.90/gperftools-2.7.90.tar.gz)

12. [LZ4 1.9.2](https://github.com/lz4/lz4)

13. [Zstd 1.4.5](https://github.com/facebook/zstd)

14. readline 6.2+

#### Fast deployment(Virtual machine image)

- Download and unzip CentOS7Dev.zip, then use the virtual machine software(Support VMware Workstations/VMware Player/Virtualbox) to import CentOS7Dev.ovf and start it.

```
Download url: https://pan.baidu.com/s/1JnxVadThOJL3K2jsscRaGA
Verification code: moqr
```

- Operating System Username:root, Password:toor.

- Before checkout openedi code, Please source /opt/devtoolset/devtoolset.bash first.

- The default configuration of the virtual machine is 4 cores and 8GB memory. Under default configuration, if multi-threaded compilation is enabled and the number of threads is greater than 8, insufficient resource may lead to build failure. Please manually modify the virtual machine hardware configuration of the virtual machine as you need.

#### Installation

1.  Clone the repository with submodules.
~~~~~~~~~~~~~~~~~~~~~
git clone --recursive https://gitee.com/niiceda/open-edi.git
~~~~~~~~~~~~~~~~~~~~~
Alternatively, you can clone the repository and the submodules in two steps.
~~~~~~~~~~~~~~~~~~~~~
git clone https://gitee.com/niiceda/open-edi.git
cd open-edi
git submodule update --init
~~~~~~~~~~~~~~~~~~~~~

2.  Build the program.
~~~~~~~~~~~~~~~~~~~~~
mkdir build   # assume you are inside the root directory of the repository
cd build
cmake ..      # you may add -DCMAKE_INSTALL_PREFIX=your/install/path to specify the build installation location, which by default is <repository>/
make          # you may add -j to turn on parallel building
make test     # optional step to run unittests
make docs     # optional step to generate HTML documentations
make install  # install 'openeda' binary and all related files in build installation location
~~~~~~~~~~~~~~~~~~~~~

#### Instructions

1.  Once installed (after make install), you can find the 'openeda' binary under 'your/install/path/bin/' directory. Run the binary <your/install/path/bin/>openeda in your design location, and try the openeda TCL commands:
~~~~~~~~~~~~~~~~~~~~~
OpenEDA> read_lef test.lef
OpenEDA> read_verilog test.v
OpenEDA> read_def test.def
OpenEDA> read_timing_library test.lib
~~~~~~~~~~~~~~~~~~~~~

2.  You can also find some test cases in demo directory, and run them with the binary just built.:
~~~~~~~~~~~~~~~~~~~~~
cd demo
tar xvf demo_case.tar.xz
cd demo_case
run.sh <your/install/path/bin/openeda>
~~~~~~~~~~~~~~~~~~~~~

#### Contribution

Welcome to contribute to our open source EDA ecosystems!
1.  Fork the repository.
2.  Create Feat_xxx branch.
3.  Commit your code.
4.  Create Pull Request to repository 'OpenEDI', branch 'dev-contrinutor'.


#### Features

1.  Open source parser to support lef, def, v, lib format design files
2.  Open source database API

#### Milestones

1.  2020/11/03 Open source EDI project went ONLINE on Gitee [https://gitee.com/niiceda/open-edi](https://gitee.com/niiceda/open-edi)
2.  2020/11/05 OpenEDI project became the recommended open source project.
3.  2020/12/01 OpenEDI project achieved the most valuable open source project [GVP](https://gitee.com/gvp)
